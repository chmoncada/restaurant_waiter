package com.charlesmoncada.restaurant.model;

import android.content.Context;

import java.util.LinkedList;

/**
 * Created by charlesmoncada on 05/12/16.
 */

public class Tables {

    private LinkedList<Table> mTables;

    public Tables(Context context) {
        mTables = new LinkedList<>();
        mTables.add(Table.get(context, 1));
        mTables.add(Table.get(context, 2));
        mTables.add(Table.get(context, 3));
        mTables.add(Table.get(context, 4));
        mTables.add(Table.get(context, 5));
    }

    public LinkedList<Table> getTables() {
        return mTables;
    }

    public Table getTable(int position) {
        return mTables.get(position);
    }

    public int getCount() {
        return mTables.size();
    }

}
