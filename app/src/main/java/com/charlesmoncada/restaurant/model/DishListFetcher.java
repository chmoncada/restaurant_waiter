package com.charlesmoncada.restaurant.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DishListFetcher {

    private static final String TAG = "DishListFetcher";

    public byte[] getUrlBytes(String urlSpec) throws IOException {

        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0 , bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public List<Dish> fetchDishes() {

        List<Dish> dishes = new ArrayList<>();

        try {
            String jsonString = getUrlString("http://www.mocky.io/v2/58464a581100008f24f3cac8");
            Log.i(TAG, "Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseDishes(dishes, jsonBody);

        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch dishes: ", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }

        return dishes;
    }

    private void parseDishes(List<Dish> dishes, JSONObject jsonBody) throws IOException, JSONException {

        JSONArray dishJsonArray = jsonBody.getJSONArray("menu");

        for (int i = 0; i < dishJsonArray.length(); i++) {
            JSONObject dishJsonObject = dishJsonArray.getJSONObject(i);

            Dish dish = new Dish();

            dish.setName(dishJsonObject.getString("name"));
            dish.setType(dishJsonObject.getString("type"));
            dish.setPrice(dishJsonObject.getInt("price"));
            dish.setImage(dishJsonObject.getString("image"));

            JSONObject alergenics = dishJsonObject.getJSONObject("alergenics");
            if (alergenics.has("01")) {
                dish.setAlergenic01(alergenics.getString("01"));
            }

            if (alergenics.has("02")) {
                dish.setAlergenic02(alergenics.getString("02"));
            }

            if (alergenics.has("03")) {
                dish.setAlergenic03(alergenics.getString("03"));
            }

            if (alergenics.has("04")) {
                dish.setAlergenic04(alergenics.getString("04"));
            }


            dishes.add(dish);
        }


    }

}
