package com.charlesmoncada.restaurant.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DishList {

    // Implement a singleton
    private static DishList sDishList;

    public List<Dish> mDishes;

    public static DishList get(Context context) {
        if (sDishList == null) {
            sDishList = new DishList(context);
        }
        return sDishList;
    }

    private DishList(Context context) {
        mDishes = new ArrayList<>();
        //Poblamos la lista hardcode de dishes
//        mDishes.add(new Dish("Causa Limeña","Entrada","01", null, 20, "milk", null, null, null));
//        mDishes.add(new Dish("Cebiche","Entrada","02", null, 25, "seafood", null, null, null));
//        mDishes.add(new Dish("Lomo Saltado","Principal","03", null, 40, null, null, null, null));
//        mDishes.add(new Dish("Aji de Gallina","Principal","04", null, 35, "gluten", "milk", null, null));
//        mDishes.add(new Dish("Arroz con Pollo","Principal","05", null, 45, "gluten", null, null, null));
//        mDishes.add(new Dish("Carapulcra","Principal","06", null, 40, "", null, null, null));
//        mDishes.add(new Dish("Mazamorra Morada","Postre","07", null, 15, "gluten", null, null, null));
//        mDishes.add(new Dish("Suspiro Limeño","Postre","08", null, 15, "eggs", "milk", null, null));
//        mDishes.add(new Dish("Chicha Morada","Bebida","09", null, 5, null, null, null, null));
//        mDishes.add(new Dish("Coca Cola","Bebida","10", null, 5, null, null, null, null));

    }


    public List<Dish> getDishes() {
        return mDishes;
    }

    public DishList setDishes(List<Dish> dishes) {
        mDishes = dishes;
        return null;
    }

    public Dish getDish(String name) {
        for (Dish dish: mDishes) {
            if (dish.getName().equals(name)) {
                return dish;
            }
        }
        return null;
    }



}
