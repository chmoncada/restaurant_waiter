package com.charlesmoncada.restaurant.model;

import java.util.LinkedList;

/**
 * Created by charlesmoncada on 04/12/16.
 */

public class Dish {

    private String mName;
    private String mType;
    private String mImage;
    private String mNote;
    private float mPrice;
    private String mAlergenic01;
    private String mAlergenic02;
    private String mAlergenic03;
    private String mAlergenic04;
    //private LinkedList<String> mAlergenics;

//    public Dish(String name, String type, String image, String note, float price, String alergenic01, String alergenic02, String alergenic03, String alergenic04) {
//        mName = name;
//        mType = type;
//        mImage = image;
//        mNote = note;
//        mPrice = price;
//        mAlergenic01 = alergenic01;
//        mAlergenic02 = alergenic02;
//        mAlergenic03 = alergenic03;
//        mAlergenic04 = alergenic04;
//    }

    public String getType() {
        return mType;
    }

    public String getAlergenic01() {
        return mAlergenic01;
    }

    public String getAlergenic02() {
        return mAlergenic02;
    }

    public String getAlergenic03() {
        return mAlergenic03;
    }

    public String getAlergenic04() {
        return mAlergenic04;
    }

    public String getName() {
        return mName;
    }

    public String getImage() {
        return mImage;
    }

    public float getPrice() {
        return mPrice;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setType(String type) {
        mType = type;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public void setAlergenic01(String alergenic01) {
        mAlergenic01 = alergenic01;
    }

    public void setAlergenic02(String alergenic02) {
        mAlergenic02 = alergenic02;
    }

    public void setAlergenic03(String alergenic03) {
        mAlergenic03 = alergenic03;
    }

    public void setAlergenic04(String alergenic04) {
        mAlergenic04 = alergenic04;
    }
}
