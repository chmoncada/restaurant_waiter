package com.charlesmoncada.restaurant.model;

import android.content.Context;
import android.support.annotation.Nullable;

import com.charlesmoncada.restaurant.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlesmoncada on 04/12/16.
 */

public class Table {

    public List<Dish> mDishes;
    private String mName;

    private static Table sMESA01;
    private static Table sMESA02;
    private static Table sMESA03;
    private static Table sMESA04;
    private static Table sMESA05;

    @Nullable
    public static Table get(Context context, int index) {
        switch (index) {
            case 1:
                if (sMESA01 == null) {
                    sMESA01 = new Table(context, index);
                }
                return sMESA01;

            case 2:
                if (sMESA02 == null) {
                    sMESA02 = new Table(context, index);
                }
                return sMESA02;
            case 3:
                if (sMESA03 == null) {
                    sMESA03 = new Table(context, index);
                }
                return sMESA03;
            case 4:
                if (sMESA04 == null) {
                    sMESA04 = new Table(context, index);
                }
                return sMESA04;
            case 5:
                if (sMESA05 == null) {
                    sMESA05 = new Table(context, index);
                }
                return sMESA05;

        }
        return null;
    }

    private Table(Context context, int index) {
        mName = context.getString(R.string.table_label)+ String.valueOf(index);
        switch (index) {
            case 1:
                mDishes = new ArrayList<>();
                //mDishes.add(new Dish("Causa Limeña","Entrada","01", null, 20, "milk", null, null, null));
                //mDishes.add(new Dish("Cebiche","Entrada","02", null, 25, "seafood", null, null, null));
                break;
            case 2:
                mDishes = new ArrayList<>();
                //mDishes.add(new Dish("Aji de Gallina","Principal","04", null, 35, "gluten", "milk", null, null));
                //mDishes.add(new Dish("Arroz con Pollo","Principal","05", null, 45, "gluten", null, null, null));
                break;
            case 3:
                mDishes = new ArrayList<>();
                break;
            case 4:
                mDishes = new ArrayList<>();
                break;
            case 5:
                mDishes = new ArrayList<>();
                break;
        }
    }

    public List<Dish> getDishes() {
        return mDishes;
    }

    public Dish getDish(String name) {
        for (Dish dish: mDishes) {
            if (dish.getName().equals(name)) {
                return dish;
            }
        }
        return null;
    }

    public float getTotalAmount() {
        float total = 0;
        for (Dish dish: mDishes) {
            total = total + dish.getPrice();
        }
        return total;
    }

    @Override
    public String toString() {
        return mName;
    }
}
