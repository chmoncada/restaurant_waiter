package com.charlesmoncada.restaurant.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.charlesmoncada.restaurant.R;
import com.charlesmoncada.restaurant.activity.DishListActivity;
import com.charlesmoncada.restaurant.model.Dish;
import com.charlesmoncada.restaurant.model.DishList;
import com.charlesmoncada.restaurant.model.Table;

import java.util.List;

public class DishListFragment extends Fragment{

    private static final String ARG_TABLE_INDEX ="table_index";

    private RecyclerView mDishRecyclerView;
    private DishAdapter mAdapter;
    private  int table_index;

    public static DishListFragment newInstance(int tableIndex) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TABLE_INDEX, tableIndex);

        DishListFragment fragment = new DishListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dish_list, container, false);

        mDishRecyclerView = (RecyclerView) view.findViewById(R.id.dish_recycler_view);
        mDishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        
        return view;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle(R.string.dish_list_title);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);

        table_index = (int) getArguments().getSerializable(ARG_TABLE_INDEX);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                getActivity().finish();
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI() {

        DishList dishList = DishList.get(getActivity());
        List<Dish> dishes = dishList.getDishes();

        mAdapter = new DishAdapter(dishes);
        mDishRecyclerView.setAdapter(mAdapter);

    }

    private class DishHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Dish mDish;

        private TextView mTitleTextView;
        private TextView mPriceTextView;
        private ImageView mImageView;
        private ImageView mAlergenic01View;
        private ImageView mAlergenic02View;
        private ImageView mAlergenic03View;
        private ImageView mAlergenic04View;

        public DishHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_name_view);
            mPriceTextView = (TextView) itemView.findViewById(R.id.list_item_price_view);
            mImageView = (ImageView) itemView.findViewById(R.id.list_item_image_view);
            mAlergenic01View = (ImageView) itemView.findViewById(R.id.list_item_alergenic1_view);
            mAlergenic02View = (ImageView) itemView.findViewById(R.id.list_item_alergenic2_view);
            mAlergenic03View = (ImageView) itemView.findViewById(R.id.list_item_alergenic3_view);
            mAlergenic04View = (ImageView) itemView.findViewById(R.id.list_item_alergenic4_view);

        }

        public void bindDish(Dish dish) {
            mDish = dish;
            mTitleTextView.setText(mDish.getName());
            mPriceTextView.setText(String.valueOf(mDish.getPrice()));

            int imageInt = Integer.parseInt(mDish.getImage());
            int imageResource = R.drawable.fig_01;
            switch (imageInt) {
                case 1:
                    imageResource = R.drawable.fig_01;
                    break;
                case 2:
                    imageResource = R.drawable.fig_02;
                    break;
                case 3:
                    imageResource = R.drawable.fig_03;
                    break;
                case 4:
                    imageResource = R.drawable.fig_04;
                    break;
                case 5:
                    imageResource = R.drawable.fig_05;
                    break;
                case 6:
                    imageResource = R.drawable.fig_06;
                    break;
                case 7:
                    imageResource = R.drawable.fig_07;
                    break;
                case 8:
                    imageResource = R.drawable.fig_08;
                    break;
                case 9:
                    imageResource = R.drawable.fig_09;
                    break;
                case 10:
                    imageResource = R.drawable.fig_10;
                    break;
            }
            mImageView.setImageResource(imageResource);

            String texto = mDish.getAlergenic01();
            int alergenic01Resource = 0;
            if (getString(R.string.eggs).equalsIgnoreCase(texto)) {
                alergenic01Resource = R.drawable.alergenic_eggs;
            } else if (getString(R.string.gluten).equalsIgnoreCase(texto)) {
                alergenic01Resource = R.drawable.alergenic_gluten;
            } else if (getString(R.string.milk).equalsIgnoreCase(texto)) {
                alergenic01Resource = R.drawable.alergenic_milk;
            } else if (getString(R.string.seafood).equalsIgnoreCase(texto)) {
                alergenic01Resource = R.drawable.alergenic_seafood;
            }
            if (alergenic01Resource != 0) {
                mAlergenic01View.setImageResource(alergenic01Resource);
            }

            String texto2 = mDish.getAlergenic02();
            int alergenic02Resource = 0;
            if (getString(R.string.eggs).equalsIgnoreCase(texto2)) {
                alergenic02Resource = R.drawable.alergenic_eggs;
            } else if (getString(R.string.gluten).equalsIgnoreCase(texto2)) {
                alergenic02Resource = R.drawable.alergenic_gluten;
            } else if (getString(R.string.milk).equalsIgnoreCase(texto2)) {
                alergenic02Resource = R.drawable.alergenic_milk;
            } else if (getString(R.string.seafood).equalsIgnoreCase(texto2)) {
                alergenic02Resource = R.drawable.alergenic_seafood;
            }
            if (alergenic02Resource != 0) {
                mAlergenic02View.setImageResource(alergenic02Resource);
            }

            String texto3 = mDish.getAlergenic03();
            int alergenic03Resource = 0;
            if (getString(R.string.eggs).equalsIgnoreCase(texto3)) {
                alergenic03Resource = R.drawable.alergenic_eggs;
            } else if (getString(R.string.gluten).equalsIgnoreCase(texto3)) {
                alergenic03Resource = R.drawable.alergenic_gluten;
            } else if (getString(R.string.milk).equalsIgnoreCase(texto3)) {
                alergenic03Resource = R.drawable.alergenic_milk;
            } else if (getString(R.string.seafood).equalsIgnoreCase(texto3)) {
                alergenic03Resource = R.drawable.alergenic_seafood;
            }
            if (alergenic03Resource != 0) {
                mAlergenic03View.setImageResource(alergenic03Resource);
            }

            String texto4 = mDish.getAlergenic04();
            int alergenic04Resource = 0;
            if (getString(R.string.eggs).equalsIgnoreCase(texto4)) {
                alergenic04Resource = R.drawable.alergenic_eggs;
            } else if (getString(R.string.gluten).equalsIgnoreCase(texto4)) {
                alergenic04Resource = R.drawable.alergenic_gluten;
            } else if (getString(R.string.milk).equalsIgnoreCase(texto4)) {
                alergenic04Resource = R.drawable.alergenic_milk;
            } else if (getString(R.string.seafood).equalsIgnoreCase(texto4)) {
                alergenic04Resource = R.drawable.alergenic_seafood;
            }
            if (alergenic04Resource != 0) {
                mAlergenic04View.setImageResource(alergenic04Resource);
            }

        }

        @Override
        public void onClick(View view) {
            Table table = Table.get(getActivity(), table_index);
            DishList dishList = DishList.get(getActivity());

            table.getDishes().add(dishList.getDish(mDish.getName()));

            Snackbar.make(getView(), getString(R.string.add_dish_snackbar_text)+ table_index, Snackbar.LENGTH_LONG).show();
        }
    }

    private class DishAdapter extends RecyclerView.Adapter<DishHolder> {

        private List<Dish> mDishes;

        public DishAdapter(List<Dish> dishes) {
            mDishes = dishes;
        }

        @Override
        public DishHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_dish, parent, false);
            return new DishHolder(view);
        }

        @Override
        public void onBindViewHolder(DishHolder holder, int position) {
            Dish dish = mDishes.get(position);
            holder.bindDish(dish);
        }

        @Override
        public int getItemCount() {
            return mDishes.size();
        }
    }

}
