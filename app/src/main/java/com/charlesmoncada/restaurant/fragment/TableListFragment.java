package com.charlesmoncada.restaurant.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.charlesmoncada.restaurant.R;
import com.charlesmoncada.restaurant.activity.TableOrderActivity;
import com.charlesmoncada.restaurant.model.Dish;
import com.charlesmoncada.restaurant.model.DishList;
import com.charlesmoncada.restaurant.model.DishListFetcher;
import com.charlesmoncada.restaurant.model.Table;
import com.charlesmoncada.restaurant.model.Tables;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.charlesmoncada.restaurant.model.DishList.*;

public class TableListFragment extends Fragment {

    private List<Dish> mDishes = new ArrayList<>();
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onTableSelected(int index);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_table_list, container, false);

        ListView list = (ListView) root.findViewById(R.id.table_list);

        // Creamos nuestro Modelo
        final Tables tables = new Tables(getActivity());

        ArrayAdapter<Table> adapter = new ArrayAdapter<Table>(getActivity(), android.R.layout.simple_list_item_1, tables.getTables());

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                Intent intent = TableOrderActivity.newIntent(getActivity(), i +1);
//                startActivity(intent);
                mCallbacks.onTableSelected(i +1);
            }
        });


        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new FetchDishesTask().execute();
    }

    public class FetchDishesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            List<Dish> restaurant_list = new DishListFetcher().fetchDishes();
            DishList dishList = DishList.get(getActivity()).setDishes(restaurant_list);

            return null;
        }
    }

}
