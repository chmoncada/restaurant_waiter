package com.charlesmoncada.restaurant.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.charlesmoncada.restaurant.R;
import com.charlesmoncada.restaurant.activity.DishDetailsActivity;
import com.charlesmoncada.restaurant.model.Dish;
import com.charlesmoncada.restaurant.model.Table;

public class DishDetailsFragment extends Fragment {

    private static final String ARG_DISH_ID = "dish_id";
    private static final String ARG_TABLE_INDEX = "table_index";

    private Dish mDish;
    private EditText mNoteField;
    private String mOldNote;
    private TextView mNameField;
    private TextView mPriceField;
    private ImageView mImageView;

    public static DishDetailsFragment newInstance(String dishID, int tableIndex) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DISH_ID, dishID);
        args.putSerializable(ARG_TABLE_INDEX, tableIndex);

        DishDetailsFragment fragment = new DishDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String nameID = (String) getArguments().getSerializable(ARG_DISH_ID);
        int table_index = (int) getArguments().getSerializable(ARG_TABLE_INDEX);

        mDish = Table.get(getActivity(), table_index).getDish(nameID);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle(R.string.dish_details_menu_title);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_dish_details, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dish_details, container, false);

        mNoteField = (EditText) root.findViewById(R.id.dish_note);

        if (mDish.getNote() != null) {
            mOldNote = mDish.getNote(); // We save old value in case the user cancel the edit of note
            mNoteField.setText(mDish.getNote());
        }
        mNoteField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // NOTHING TO DO
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Captures the text input and put it inside mDish
                mDish.setNote(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // NOTHING TO DO
            }
        });

        mNameField = (TextView) root.findViewById(R.id.dish_name);
        mNameField.setText(mDish.getName());

        mPriceField = (TextView) root.findViewById(R.id.dish_price);
        mPriceField.setText(String.valueOf(mDish.getPrice()));

        mImageView = (ImageView) root.findViewById(R.id.dish_image);
        int imageInt = Integer.parseInt(mDish.getImage());
        int imageResource = R.drawable.fig_01;
        switch (imageInt) {
            case 1:
                imageResource = R.drawable.fig_01;
                break;
            case 2:
                imageResource = R.drawable.fig_02;
                break;
            case 3:
                imageResource = R.drawable.fig_03;
                break;
            case 4:
                imageResource = R.drawable.fig_04;
                break;
            case 5:
                imageResource = R.drawable.fig_05;
                break;
            case 6:
                imageResource = R.drawable.fig_06;
                break;
            case 7:
                imageResource = R.drawable.fig_07;
                break;
            case 8:
                imageResource = R.drawable.fig_08;
                break;
            case 9:
                imageResource = R.drawable.fig_09;
                break;
            case 10:
                imageResource = R.drawable.fig_10;
                break;
        }
        mImageView.setImageResource(imageResource);


        return root;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_save:
                Toast.makeText(getActivity(), "Nota Actualizada", Toast.LENGTH_SHORT)
                       .show();
                getActivity().finish();
                return true;
            case android.R.id.home:
                Toast.makeText(getActivity(), "NO SE GRABARAN LOS CAMBIOS", Toast.LENGTH_SHORT)
                        .show();
                mDish.setNote(mOldNote); // we put old value to mDish in case we cancel
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
