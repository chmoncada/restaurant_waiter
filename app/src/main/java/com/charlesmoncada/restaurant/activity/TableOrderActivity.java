package com.charlesmoncada.restaurant.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.charlesmoncada.restaurant.fragment.TableOrderFragment;

public class TableOrderActivity extends SingleFragmentActivity {

    private static final String EXTRA_TABLE_INDEX = "EXTRA_TABLE_INDEX";

    public static Intent newIntent(Context packageContent, int index) {
        Intent intent = new Intent(packageContent, TableOrderActivity.class);
        intent.putExtra(EXTRA_TABLE_INDEX, index);
        return intent;
    }


    @Override
    protected Fragment createFragment() {

        int tableIndex = (int) getIntent().getSerializableExtra(EXTRA_TABLE_INDEX);
        return  TableOrderFragment.newInstance(tableIndex);
    }
}
