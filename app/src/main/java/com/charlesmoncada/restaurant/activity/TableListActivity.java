package com.charlesmoncada.restaurant.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.charlesmoncada.restaurant.R;
import com.charlesmoncada.restaurant.fragment.TableListFragment;
import com.charlesmoncada.restaurant.fragment.TableOrderFragment;
import com.charlesmoncada.restaurant.model.Table;


public class TableListActivity extends SingleFragmentActivity implements TableListFragment.Callbacks {
    @Override
    protected Fragment createFragment() {
        return new TableListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    public void onTableSelected(int index) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = TableOrderActivity.newIntent(this, index);
            startActivity(intent);
        } else {
            Fragment tableOrder = TableOrderFragment.newInstance(index);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, tableOrder)
                    .commit();
        }
    }
}
