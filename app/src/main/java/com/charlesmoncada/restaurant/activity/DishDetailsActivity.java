package com.charlesmoncada.restaurant.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.charlesmoncada.restaurant.fragment.DishDetailsFragment;

public class DishDetailsActivity extends SingleFragmentActivity {

    private static final String EXTRA_DISH_ID = "EXTRA_DISH_ID";
    private static final String EXTRA_TABLE_INDEX = "EXTRA_TABLE_INDEX";

    public static Intent newIntent(Context packageContent, String nameID, int index) {
        Intent intent = new Intent(packageContent, DishDetailsActivity.class);
        intent.putExtra(EXTRA_DISH_ID, nameID);
        intent.putExtra(EXTRA_TABLE_INDEX, index);
        return intent;
    }


    @Override
    protected Fragment createFragment() {
        String dishID = (String) getIntent().getSerializableExtra(EXTRA_DISH_ID);
        int tableIndex = (int) getIntent().getSerializableExtra(EXTRA_TABLE_INDEX);

        return DishDetailsFragment.newInstance(dishID,tableIndex);
    }
}
